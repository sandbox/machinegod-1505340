// $Id$

mgEveAuth - Eve online authentication layer for Drupal
mgEveItemPrices - Eve Online price item price caching via eve-central.com
mgEveMatTracts - Quick pricing and verification of materials contracts

----------------------------------------------------


Known incompatibilities
-----------------------

No incompatibilities with other modules are known to this date.


Maintainers
-----------
mgEveMatTracts is written by Michael Lehman - Drakos Wraith in game.


